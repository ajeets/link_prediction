from pyspark.sql import SparkSession
from os.path import abspath, curdir, join
from pyspark.sql import Row
from graphframes import *
from math import log
import operator
import logging

# run with (spark 2.0): spark-submit --packages graphframes:graphframes:0.2.0-spark2.0-s_2.11 main.py


def get_neighbours(root_node):
    query = "SELECT dst FROM edges_table WHERE src = '%s'" % root_node
    nbh_df1 = spark.sql(query)

    query = "SELECT src FROM edges_table WHERE dst = '%s'" % root_node
    nbh_df2 = spark.sql(query)

    nbh_df = nbh_df1.union(nbh_df2).dropDuplicates()  # union: so column names becomes dst (the first one)
    return nbh_df


def get_raw_predictions(root_node):
    root_nbh_df = get_neighbours(root_node)
    root_nbh_nodes = root_nbh_df.rdd.map(lambda row: row.dst).collect()

    raw_predictions = set()

    for node in root_nbh_nodes:
        node_df = get_neighbours(node)
        raw_predictions = raw_predictions.\
            union(set(node_df.rdd.map(lambda row: row.dst).collect()))

    # Root's NBRs' NBRs - Root's NBRs - Root
    raw_predictions = raw_predictions.difference(set(root_nbh_nodes)).difference({root_node})

    return raw_predictions  # eligible candidates for root


def determine_score(n):
    score = float(0)
    try:
        score = log(n)**-1
    except Exception as e:
        logging.error(e)
    return score


def get_adamicadar_score(neighbours_x_node, y_node):  # set(), int
    neighbours_y_node_df = get_neighbours(y_node)
    neighbours_y_node = set(neighbours_y_node_df.rdd.map(lambda row: row.dst).collect())
    common_neighbours_xy = neighbours_y_node.intersection(neighbours_x_node)

    score = float(0)

    for node in common_neighbours_xy:
        node_df = get_neighbours(node)
        score += determine_score(node_df.count())

    return score


def rank_raw_predictions(root_node):
    raw_predictions = get_raw_predictions(root_node=root_node)
    nbh_df = get_neighbours(root_node=root_node)
    nbh_list = nbh_df.rdd.map(lambda row: row.dst).collect()

    raw_predictions_score = []

    for node in raw_predictions:
        node_score = get_adamicadar_score(neighbours_x_node=set(nbh_list), y_node=node)
        raw_predictions_score.append((node, node_score))

    raw_predictions_rank = sorted(raw_predictions_score, key=operator.itemgetter(1), reverse=True)
    return raw_predictions_rank  # list of tuples (node, node_score): sorted by score


def process_the_nodes():
    vertices_list = v_DF.rdd.map(lambda row: row.id).collect()
    for vertex in vertices_list:
        raw_predictions_rank = rank_raw_predictions(root_node=vertex)
        print vertex
        print raw_predictions_rank


if __name__ == '__main__':

    spark = SparkSession.builder.master("local[*]").appName("Link Prediction").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    working_directory = abspath(curdir)
    file_path = join(working_directory, 'resources/edges.txt')

    edgesRDD = spark.read.text(file_path).rdd.map(lambda r: r[0])
    edges_parts_RDD = edgesRDD.map(lambda line: line.split(" "))
    edges_row_RDD = edges_parts_RDD.\
        map(lambda p: Row(src=str(p[0].strip()), dst=str(p[1].strip())))
    edgesDF = spark.createDataFrame(edges_row_RDD)
    edgesDF.createOrReplaceTempView("edges_table")

    e_DF = edgesDF.select(edgesDF.src, edgesDF.dst)  # to fix the order of columns (src, dst) in the DataFrame
    e_DF.cache()  # cached in the MEMORY

    v0_RDD = edges_parts_RDD.map(lambda p: (str(p[0].strip()),))
    v1_RDD = edges_parts_RDD.map(lambda p: (str(p[1].strip()),))
    v_RDD = v0_RDD.union(v1_RDD)
    v_DF = spark.createDataFrame(v_RDD, schema=["id"]).dropDuplicates(["id"])
    v_DF.cache()  # cached in the MEMORY

    g = GraphFrame(v_DF, e_DF)
    process_the_nodes()

    spark.stop()

