# README #


### Link Prediction in Social Network ###

This repository provides a quick example to link prediction problem. It is written in Python and uses Apache Spark computing framework.


### Requires ###

* Python 2.7+
* [Apache Spark 2.0.0](http://spark.apache.org/downloads.html)
* [GraphFrames 0.2.0](https://spark-packages.org/package/graphframes/graphframes) for spark2.0-s_2.11


### Running with `spark-submit` ###

`cd ~/linkprediction`

`spark-submit --packages graphframes:graphframes:0.2.0-spark2.0-s_2.11 ./main.py`


### Demo ###
[Python Notebook](https://databricks-prod-cloudfront.cloud.databricks.com/public/4027ec902e239c93eaaa8714f173bcfc/6291720762367683/1179728203604863/6637403919549794/latest.html)
